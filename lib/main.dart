import 'package:flutter/material.dart';
import 'package:heliostest/views/user_list.dart';
import 'package:heliostest/view-models/UserVM.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserVM>(
          create: (context) => UserVM(),
        ),
      ],
      child: MaterialApp(
        title: 'Helios demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: UsersList(),
      ),
    );
  }
}
