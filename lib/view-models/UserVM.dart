import 'package:heliostest/models/User.dart';
import 'package:heliostest/api/userAPI.dart';
import 'package:flutter/material.dart';

class UserVM extends ChangeNotifier {
  List<User> users = [];

  UserVM() {
    getUsers();
  }

  void refreshUsers() async {
    users.clear();
    notifyListeners();
    getUsers();
  }

  void getUsers() async {
    List<User> newUsers = await UserAPI.getUsers();
    for (User user in newUsers) {
      users.add(user);
    }
    notifyListeners();
  }
}
