class User {
  User({
    this.firstName,
    this.lastName,
    this.photoUrl,
    this.phoneNumber,
    this.email,
  });

  String firstName;
  String lastName;
  String photoUrl;
  String phoneNumber;
  String email;

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      email: json['email'],
      phoneNumber: json['phone'],
      firstName: json['name']['first'],
      lastName: json['name']['last'],
      photoUrl: json['picture']['large'],
    );
  }
}
