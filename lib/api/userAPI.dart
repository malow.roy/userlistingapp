import 'dart:convert';

import 'package:heliostest/models/User.dart';
import 'package:http/http.dart' as http;

class UserAPI {
  static Future<List<User>> getUsers() async {
    List<User> users = [];
    var url = Uri.https('www.randomuser.me', '/api/', {'results': '20'});
    final response = await http.get(url);

    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse = jsonDecode(response.body);
      List<dynamic> data = jsonResponse["results"];
      for (final o in data) {
        users.add(User.fromJson(o));
      }
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
    return users;
  }
}
