import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:heliostest/view-models/UserVM.dart';
import 'package:heliostest/models/User.dart';
import 'package:heliostest/widgets/user_list_tile.dart';

class UsersList extends StatefulWidget {
  @override
  _UsersListState createState() => _UsersListState();
}

class _UsersListState extends State<UsersList> {
  ScrollController _scrollController = new ScrollController();

  List<Widget> _getUserTiles(List<User> users) {
    List<Widget> userTiles = [];
    for (User user in users) {
      userTiles.add(UserListTile(user: user));
    }
    userTiles.add(
      Padding(
        padding: const EdgeInsets.all(30.0),
        child: Center(
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.black),
          ),
        ),
      ),
    );
    return userTiles;
  }

  @override
  void initState() {
    _scrollController
      ..addListener(() {
        var triggerFetchMoreSize =
            0.9 * _scrollController.position.maxScrollExtent;

        if (_scrollController.position.pixels > triggerFetchMoreSize) {
          Provider.of<UserVM>(context, listen: false).getUsers();
        }
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 20, top: 60, bottom: 20, right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Users list',
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w700,
                      color: Colors.black87,
                    ),
                  ),
                  IconButton(
                      icon: Icon(
                        Icons.refresh,
                        color: Colors.black26,
                        size: 30,
                      ),
                      onPressed: () {
                        Provider.of<UserVM>(context, listen: false)
                            .refreshUsers();
                      })
                ],
              ),
            ),
            Expanded(
              child: Consumer<UserVM>(
                builder: (context, vm, child) {
                  return vm.users.isNotEmpty
                      ? ListView(
                          controller: _scrollController,
                          children: _getUserTiles(vm.users),
                        )
                      : Center(
                          child: CircularProgressIndicator(
                            valueColor:
                                new AlwaysStoppedAnimation<Color>(Colors.black),
                          ),
                        );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
