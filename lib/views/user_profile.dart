import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:heliostest/models/User.dart';

class UserProfile extends StatelessWidget {
  final User user;

  UserProfile({this.user});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding:
              const EdgeInsets.only(left: 20, top: 60, bottom: 20, right: 20),
          child: Stack(
            children: [
              IconButton(
                icon: Icon(Icons.arrow_back_ios_rounded, size: 30),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: 200.0,
                      height: 200.0,
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            blurRadius: 5.0,
                            spreadRadius: 2.0,
                          ),
                        ],
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: new NetworkImage(user.photoUrl),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    Text(
                      user.firstName + ' ' + user.lastName,
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.email,
                          color: Colors.black45,
                        ),
                        SizedBox(width: 10),
                        Text(
                          user.email,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Colors.black45),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.phone,
                          color: Colors.black45,
                        ),
                        SizedBox(width: 10),
                        Text(
                          user.phoneNumber,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: Colors.black45),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
